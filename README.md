# gitsh

A simple program that encourages using the shim
for `sh` on Windows where git has been installed by [Chocolatey](https://chocolatey.org/)

[![pipeline status](https://gitlab.com/tmarble/gitsh/badges/master/pipeline.svg)](https://gitlab.com/tmarble/gitsh/-/commits/master)

## License

Copyright © 2020 Tom Marble

Licensed under the [MIT](https://opensource.org/licenses/MIT) [LICENSE](LICENSE)
