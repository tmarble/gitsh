; gitsh.nsi
;
;--------------------------------

; The name of the installer
Name "Gitsh"

; The file to write
OutFile "install_gitsh.exe"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

; Build Unicode installer
Unicode True

; The default installation directory
InstallDir $PROGRAMFILES64\Gitsh

; Registry key to check for directory (so if you install again, it will
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Gitsh" "Install_Dir"

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Gitsh (required)"
  SectionIn RO

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Put file there
  File "target/x86_64-pc-windows-gnu/release/gitsh.exe"

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\Gitsh "Install_Dir" "$INSTDIR"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Gitsh" "DisplayName" "Gitsh"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Gitsh" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Gitsh" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Gitsh" "NoRepair" 1
  WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Gitsh"
  CreateShortcut "$SMPROGRAMS\Gitsh\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortcut "$SMPROGRAMS\Gitsh\Gitsh.lnk" 'C:\Windows\System32\cmd.exe' '/K "$INSTDIR\gitsh.exe"' "$INSTDIR\gitsh.exe" 0
  ; CreateShortcut "$SMPROGRAMS\Gitsh\Gitsh.lnk" "$INSTDIR\gitsh.exe" "" "$INSTDIR\gitsh.exe" 0

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"

  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Gitsh"
  DeleteRegKey HKLM SOFTWARE\Gitsh

  ; Remove files and uninstaller
  Delete $INSTDIR\gitsh.exe
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Gitsh\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\Gitsh"
  RMDir "$INSTDIR"

SectionEnd
