﻿$ErrorActionPreference = 'Stop'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url64 = "https://gitlab.com/api/v4/projects/18083956/jobs/507866106/artifacts/install_gitsh.exe"
$checksumType64 = "sha256"
$checksum64 = '36d70cd16f9f4d7f908a75c3fc7625f4e46888835e49d2e14158b773f46384e4'

$packageArgs = @{
    packageName    = $env:ChocolateyPackageName
    fileType       = 'exe'
    silentArgs     = "/S"
    Url64bit       = $url64
    ChecksumType64 = $checksumType64
    Checksum64     = $checksum64
}

Install-ChocolateyPackage @packageArgs

$ProgramFiles = "$(Get-EnvironmentVariable -Name 'ProgramFiles' -Scope 'Process')"
$shPath = Join-Path $ProgramFiles "Git\bin\sh.exe"

Install-BinFile -Name 'sh' -Path $shPath


